# vtags -- ultimate tags solution for Haskell development

## Background

Tags is the most simple and reliable option when navigating source code.
In short, tag is specification where given symbol is defined -- in what
file, on what line. They can be generated very accurately, with the very
compiler, compiling source files, or very fast, using nothing more, then
regular expressions and assumtions, like name of function always start
on column 0.

No matter how, once tags file is generated, it is very reliable, without
any moving parts.

For many programming languages `exuberant-ctags` (also known `ctags`)[1]
is absolute solution. Haskell, which is not supported, has two own
implementations:

 1. `hasktags` -- very fast implementation.
 2. `hothasktags` -- very accurate implementation, using same parser, as
    GHC

In either way, where does `vtags` comes it?

## Tagging libraries

Both `hasktags` and `hothasktags` generate tags file for current
project, but often it is desirable to take a look at source of library
function, whether because documentation is poor or lacking, or because
code is always much more accurate than words. Enter `vtags`.

There is no command line options, just invoke `vtags` somewhere in your
project. It will do following things:

 1. Locate stack.yaml file
 2. Read list of packages, and generate tags file for them.
 3. Understand what are your project dependencies, what are their versions,
    and generate tags file for them.
 4. Merge mentioned tags files into single sorted one, and place it next
    to `stack.yaml` file.

Now you can enjoy fast, reliable source code navigation, that require
very little resources from your computer.

## Limitations

As you guessed from description above, `vtags` assumes that you use
`stack` as build tool. If you do not, you should a least give it a try.
