{-# LANGUAGE ForeignFunctionInterface #-}
-- |
-- Module      :  VTags.Merge
-- Copyright   :  Dmitry Bogatov 2018
-- License     :  GPL-3
--
-- Maintainer  :  KAction@gnu.org
-- Portability :  POSIX
--
-- Module, containing functions to merge two tags files efficently,
-- using fact, that they are sorted alphabetically.
--

module VTags.Merge
  ( hMerge
  , fileMerge
  ) where

import Foreign.C
import System.IO (Handle, withFile, IOMode(..))
import System.Posix.IO (handleToFd)
import System.Posix.Types (Fd(..))

foreign import ccall "merge_lines"
  c_merge_lines :: CInt -> CInt -> CInt -> IO ()

hMerge :: (Handle, Handle) -> Handle -> IO ()
hMerge (in1, in2) out = do
  Fd inF1 <- handleToFd in1
  Fd inF2 <- handleToFd in2
  Fd outF <- handleToFd out
  c_merge_lines inF1 inF2 outF

fileMerge :: (FilePath, FilePath) -> FilePath -> IO ()
fileMerge (in1, in2) out
  = withFile in1 ReadMode $ \hIn1 ->
      withFile in2 ReadMode $ \hIn2 ->
        withFile out WriteMode $ \hOut -> hMerge (hIn1, hIn2) hOut
