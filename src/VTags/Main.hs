{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
-- |
-- Module      :  VTags.Main
-- Copyright   :  Dmitry Bogatov 2018
-- License     :  GPL-3
--
-- Maintainer  :  KAction@gnu.org
-- Portability :  POSIX
--
-- This module defines entry point for @vtags@ program.
--

module VTags.Main
  ( main
  ) where

import VTags.Merge (fileMerge)
import Control.Monad.Extra (unlessM)
import Data.List (sort, isInfixOf)
import Data.List.Split (splitOn)
import Development.Shake
import Development.Shake.FilePath

import Control.Monad (forM_)
import Control.Exception (throwIO)
import System.Directory
  ( makeAbsolute
  , getHomeDirectory
  )
import System.Exit (ExitCode(..))
import System.FilePath.Posix ((</>))

data Dependency
  = Dependency { _package :: String
               , _version :: String
               }

splitDependency :: String -> IO Dependency
splitDependency line =
  case words line of
    [package, version] -> pure $ Dependency package version
    _                  -> throwIO.userError $ "bad line: " ++ line

dependencyTags :: FilePath -> Dependency -> FilePath
dependencyTags home (Dependency{..}) =
  home </> ".stack/.vtags" </> _package ++ "-" ++ _version </> "tags"

getSourcesIn :: FilePath -> Action [FilePath]
getSourcesIn directory = do
  sources' <- getDirectoryFiles directory ["//*.hs"]
  mapM (liftIO.makeAbsolute.(directory </>)) sources'

-- FIXME: Inefficent -- it does not use fact, that
-- tags files are already sorted.
mergeLines :: [FilePath] -> Action [String]
mergeLines paths =
  sort.concat <$> mapM readFileLines paths

main :: IO ()
main = do
  home <- getHomeDirectory
  let options = shakeOptions
        { shakeFiles = home </> ".stack/.vtags/.buildinfo" }
  shakeArgs options $ do
    want ["tags"]
    "tags" %> \out -> do
      let internal = ".git/.tags/internal"
          external = ".git/.tags/external"
      need [internal, external]
      traced "merging tags" $ fileMerge (internal, external) out

    ".git/.tags/dependencies.txt" %> \out -> do
      need ["stack.yaml"]
      Stdout txt <- cmd "stack ls dependencies --depth 1"
      Stdout txt0 <- cmd "stack ls dependencies --depth 0"
      let txt' = lines txt
          txt0' = lines txt0
      writeFileLines out (filter (not. (`elem` txt0')) txt')

    ".git/.tags/internal" %> \out -> do
      sources' <- getSourcesIn "."
      let sources = filter (not.isInfixOf "/.stack-works/") sources'
      need sources
      Stdout txt <- cmd "hothasktags" sources
      writeFile' out txt
    ".git/.tags/external" %> \out -> do
      deps <- readFileLines ".git/.tags/dependencies.txt"
                >>= mapM (liftIO.splitDependency)
      let tags = map (dependencyTags home) deps
      mergeLines tags >>= writeFileLines out

    (home </> ".stack/.vtags//*/tags") %> \out -> do
      let directory = takeDirectory out
          target    = takeFileName directory
      cmd "rmdir" directory >>= \case
        (_, ExitSuccess) ->
          cmd "stack unpack --to" (home </> ".stack/.vtags") target
        (Stderr _err, ExitFailure _) ->
          let _ = _err :: String in pure ()
      sources <- getSourcesIn directory
      (exit, Stdout txt, Stderr _err) <- cmd "hothasktags" sources
      let _ = _err :: String
      case exit of
        ExitSuccess -> writeFile' out txt
        ExitFailure _ -> cmd_ "hasktags -x -R -c -o" out directory
