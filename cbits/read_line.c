#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "read_line.h"

enum { buffer_size = READ_LINE_BUFFER_SIZE };

static inline size_t
get_available_space(const struct read_line_it *it)
{
	return it->bytes + it->buffer - it->start;
}

void
read_line_init (struct read_line_it *it, int fd)
{
	it->fd = fd;
	it->start = it->buffer;
	it->bytes = 0; // No data read yet.
	it->buffer[buffer_size] = '\0';
}

char *
read_line_get(struct read_line_it *it)
{
	char *old_start;
	char *end;
	int space;

	if (it->start == it->buffer + it->bytes) {
		it->start = it->buffer;
		it->bytes = 0;
	}

	if (it->bytes == 0) {
		ssize_t bytes = read(it->fd, it->buffer, buffer_size);
		if (bytes <= 0)
			return NULL;
		it->bytes = bytes;
	}

	space = get_available_space(it); // space > 0 here
	assert(space > 0);

	end = memchr(it->start, '\n', space);
	if (end) {
		old_start = it->start;
		*end = '\0';
		it->length = end - it->start;
		it->start = end + 1;
		return old_start;
	} else {
		if (it->start == it->buffer) {
			// Input line is too long. We are forced to
			// break it.
			old_start = it->start;
			it->start = it->buffer + it->bytes;
			it->length = buffer_size;
			return old_start;
		} else {
			ssize_t bytes;
			memmove(it->buffer, it->start, space);
			it->start = it->buffer;
			bytes = read(it->fd, it->start + space, buffer_size - space);
			if (bytes <= 0) {
				it->bytes = space;
			} else {
				it->bytes = space + bytes;
			}
			return read_line_get(it);
		}
	}
}

char *
read_line_leftover(struct read_line_it *it, size_t *len)
{
	*len = get_available_space(it);
	return it->start;
}
