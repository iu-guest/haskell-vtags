/*************************************************************************/
/* Read lines from file descriptor without allocations                   */
/* Copyright © 2018 Dmitry Bogatov                                       */
/*                                                                       */
/* This program is free software: you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation, either version 3 of the License, or     */
/* (at your option) any later version.                                   */
/*                                                                       */
/* This program is distributed in the hope that it will be useful,       */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/* GNU General Public License for more details.                          */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */
/*************************************************************************/

#ifndef READ_LINE_BUFFER_SIZE
#define READ_LINE_BUFFER_SIZE 1024
#endif

struct read_line_it {
	char *start;
	size_t length;
	int fd;
	char buffer[READ_LINE_BUFFER_SIZE + 1];
	ssize_t bytes;
};

void read_line_init(struct read_line_it *it, int fd);
char * read_line_get(struct read_line_it *it);
char * read_line_leftover(struct read_line_it *it, size_t *len);
